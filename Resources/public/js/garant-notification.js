(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Created by Alsheuski Alexei on 11/07/16.
 * File: garant-notification.js
 */

'use strict';

var GarantNotificationVendor = function () {

    var isConnected = false;

    var listeners = [],
        currentSession = void 0;

    /**
     * Сохранить объект сессии
     * @param session - объект сессии
     */
    var setCurrentSession = function setCurrentSession(session) {

        if (session) {
            currentSession = session;
        } else {
            console.log('Session in undefined!');
        }
    };

    /**
     * Получить объект сессии
     * @returns {*}
     */
    var getCurrentSession = function getCurrentSession() {

        if (currentSession) {
            return currentSession;
        } else {
            console.log('Session in undefined!');
        }
    };

    /**
     * Добавление внешнего обработчика события
     * @param eventName - название события
     * @param callback - колбэк
     */
    var on = function on(eventName, callback) {

        listeners[eventName] = listeners[eventName] || [];
        listeners[eventName].push(callback);
    };

    /**
     * Подписываемся на список каналов нотификаций
     * @param notificationsList
     * @private
     */
    function _handleNotifications(notificationsList) {

        if (isConnected) {

            notificationsList.forEach(function (item) {

                getCurrentSession().subscribe(item, function (topic, res) {

                    // Выполняем колбэки подписчиков
                    listeners[item].forEach(function (callback) {
                        callback(res);
                    });
                });
            });
        }
    }

    /**
     * Получение списка топиков для оформления подписки на новые нотификации
     * @param uri
     * @returns {*}
     */
    function getTopicsForSubsciption(uri) {
        return getCurrentSession().call(uri);
    }

    /**
     * Отправка подтверждения прочтения нотификации
     * @returns {*}
     */
    function confirmReadingNotification(notificationId) {
        return getCurrentSession().call(Routing.generate('notification_confirm_read'), JSON.stringify([notificationId]));
    }

    /**
     * Подключение к notification-серверу
     * @param settings
     */
    function connect(settings) {

        if (typeof window.ab !== 'undefined') {

            /**
             * Обработчки успешного подключения
             * @param session
             * @private
             */
            var _onSuccess = function _onSuccess(session) {

                setCurrentSession(session);

                console.log('Соединение с сервером нотификаций успешно установлено!');

                isConnected = true;

                // Выполняем колбэки подписчиков
                listeners['connected'].forEach(function (callback) {
                    callback();
                });

                // подписываемся на каналы нотификаций
                _handleNotifications(settings.topicsList);

                // session.call(settings.topicListsUri).then(function (result) {
                //
                //
                //     result = JSON.parse(result);
                //
                //     console.log(result);
                //
                //     $.each(result.response, function (index, value) {
                //
                //         console.log(value);
                //
                //         session.subscribe(value, function (topic, data) {
                //
                //             console.log(JSON.parse(data).response);
                //
                //         });
                //     });
                //
                //     session.call(settings.topicUnsetNotificationUri).then(function (result) {
                //
                //         console.log($.parseJSON(data));
                //
                //     }, function (error) {
                //         // enpty
                //     });
                // });
            };

            /**
             * Обработчки дисконнекта
             * @private
             */
            var _onClose = function _onClose() {

                isConnected = false;

                console.log('connection closed.');
            };

            ab.connect('ws://' + settings.wsuri, _onSuccess, _onClose(), settings.connectionOptions);
        } else {
            console.error('Autobahn is undefined!');
        }
    }

    return {
        connect: connect,
        on: on,
        getTopicsForSubsciption: getTopicsForSubsciption,
        confirmReadingNotification: confirmReadingNotification
    };
}();

window.GarantNotificationVendor = GarantNotificationVendor;

},{}]},{},[1]);
