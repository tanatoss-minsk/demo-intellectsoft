/**
 * Created by Alsheuski Alexei on 11/07/16.
 * File: garant-notification.js
 */

'use strict';

var GarantNotificationVendor = function () {

    let isConnected = false;

    let listeners = [],
        currentSession;


    /**
     * Сохранить объект сессии
     * @param session - объект сессии
     */
    let setCurrentSession = function (session) {

        if (session) {
            currentSession = session;
        } else {
            console.log('Session in undefined!');
        }
    }

    /**
     * Получить объект сессии
     * @returns {*}
     */
    let getCurrentSession = function () {

        if (currentSession) {
            return currentSession;
        } else {
            console.log('Session in undefined!');
        }
    }

    /**
     * Добавление внешнего обработчика события
     * @param eventName - название события
     * @param callback - колбэк
     */
    let on = function (eventName, callback) {

        listeners[eventName] = listeners[eventName] || [];
        listeners[eventName].push(callback);

    }

    /**
     * Подписываемся на список каналов нотификаций
     * @param notificationsList
     * @private
     */
    function _handleNotifications(notificationsList) {

        if (isConnected) {

            notificationsList.forEach(item => {

                getCurrentSession().subscribe(item, (topic, res) => {

                    // Выполняем колбэки подписчиков
                    listeners[item].forEach(function (callback) {
                        callback(res);
                    })
                })

            })
        }
    }

    /**
     * Получение списка топиков для оформления подписки на новые нотификации
     * @param uri
     * @returns {*}
     */
    function getTopicsForSubsciption(uri) {
        return getCurrentSession().call(uri);
    }

    /**
     * Отправка подтверждения прочтения нотификации
     * @returns {*}
     */
    function confirmReadingNotification(notificationId) {
        return getCurrentSession().call(Routing.generate('notification_confirm_read'), notificationId);
    }

    /**
     * Подключение к notification-серверу
     * @param settings
     */
    function connect(settings) {

        if (typeof window.ab !== 'undefined') {


            /**
             * Обработчки успешного подключения
             * @param session
             * @private
             */
            let _onSuccess = function (session) {

                setCurrentSession(session);

                console.log('Соединение с сервером нотификаций успешно установлено!');

                isConnected = true;

                // Выполняем колбэки подписчиков
                listeners['connected'].forEach(function (callback) {
                    callback();
                });

                // подписываемся на каналы нотификаций
                _handleNotifications(settings.topicsList);

                // session.call(settings.topicListsUri).then(function (result) {
                //
                //
                //     result = JSON.parse(result);
                //
                //     console.log(result);
                //
                //     $.each(result.response, function (index, value) {
                //
                //         console.log(value);
                //
                //         session.subscribe(value, function (topic, data) {
                //
                //             console.log(JSON.parse(data).response);
                //
                //         });
                //     });
                //
                //     session.call(settings.topicUnsetNotificationUri).then(function (result) {
                //
                //         console.log($.parseJSON(data));
                //
                //     }, function (error) {
                //         // enpty
                //     });
                // });

            };

            /**
             * Обработчки дисконнекта
             * @private
             */
            let _onClose = function () {

                isConnected = false;

                console.log('connection closed.');
            };

            ab.connect('ws://' + settings.wsuri,
                _onSuccess,
                _onClose(),
                settings.connectionOptions
            );


        } else {
            console.error('Autobahn is undefined!');
        }
    }

    return {
        connect: connect,
        on: on,
        getTopicsForSubsciption: getTopicsForSubsciption,
        confirmReadingNotification: confirmReadingNotification
    };
}();

window.GarantNotificationVendor = GarantNotificationVendor;
