<?php

namespace Garant\ECM\Bundle\NotificationBundle\Traits;

/**
 * Created by PhpStorm.
 * User: archer
 * Date: 2.6.16
 * Time: 13.29
 */
trait EventTypeTrait
{

    protected $type;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}