<?php

namespace Garant\ECM\Bundle\NotificationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ResolveTargetEntityPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('doctrine.orm.listeners.resolve_target_entity')) {
            throw new \RuntimeException('doctrine.orm.listeners.resolve_target_entity must be defined');
        }
        $def = $container->findDefinition('doctrine.orm.listeners.resolve_target_entity');

        $def->addMethodCall(
            'addResolveTargetEntity',
            array('Garant\ECM\DomainModel\Notification\Model\Notification',
                $container->getParameter('garant_ecm_notification.notification_entity_class'), [])
        );
        $def->addMethodCall(
            'addResolveTargetEntity',
            array('\Garant\ECM\DomainModel\Organisation\Model\BaseEmployee',
                $container->getParameter('garant_ecm_notification.employee_entity_class'), [])
        );
    }
}