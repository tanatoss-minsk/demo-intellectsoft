<?php

namespace Garant\ECM\Bundle\NotificationBundle\Topic\Generator\Strategy;

use Garant\ECM\DomainModel\Organisation\Model\BaseEmployee as BaseEmployee;
use Garant\ECM\Bundle\NotificationBundle\Topic\Generator\StrategyInterface;

/**
 * Class EmployeeStrategy
 * @package Garant\ECM\Bundle\NotificationBundle\Topic\Generator\Strategy
 */
class EmployeeStrategy implements StrategyInterface
{
    /**
     * @var Employee
     */
    protected $employee;

    /**
     * @param Employee $employee
     */
    public function __construct(BaseEmployee $employee)
    {
        $this->employee = $employee;
    }

    /**
     * @return mixed
     */
    public function getParameters()
    {
        return [
            [
                'employee' => $this->employee->getId()
            ]
        ];
    }

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return 'notification_topic';
    }
}