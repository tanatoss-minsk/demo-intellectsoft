<?php

namespace Garant\ECM\Bundle\NotificationBundle\Topic;

use Garant\ECM\Bundle\NotificationBundle\Topic\Generator\StrategyInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Routing\Router;

/**
 * Class Generator
 * @package Garant\ECM\Bundle\NotificationBundle\Topic
 */
class Generator
{
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @param Router $router
     */
    public function __construct(Registry $registry, Router $router)
    {
        $this->registry = $registry;
        $this->router   = $router;
    }

    /**
     * @param StrategyInterface $strategy
     * @return array
     */
    public function generate(StrategyInterface $strategy)
    {
        $topics = array();

        $parameters = $strategy->getParameters();
        foreach ($parameters as $parameter) {

            $topics[] = $this->router->generate($strategy->getRoute(), $parameter);
        }

        return $topics;
    }
}