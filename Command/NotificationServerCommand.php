<?php

namespace Garant\ECM\Bundle\NotificationBundle\Command;

use Garant\ECM\Bundle\NotificationBundle\Wamp\Socket;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use React\EventLoop\Factory as LoopFactory;
use React\ZMQ\Context as ZMQContext;
use Ratchet\App as Application;
use Ratchet\Session\SessionProvider;
use Ratchet\Wamp\WampServer;

use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;

/**
 * Class NotificationServerCommand
 * @package Garant\ECM\Bundle\NotificationBundle\Command
 */
class NotificationServerCommand extends ContainerAwareCommand
{
    const COMMAND_NAME = 'garant:ecm:notification:server';

    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('Websocket server for notification system')
            ->addOption('host',    'hs', InputOption::VALUE_OPTIONAL, 'Host', null)
            ->addOption('port',    'p',  InputOption::VALUE_OPTIONAL, 'Port', null)
            ->addOption('address', 'a',  InputOption::VALUE_OPTIONAL, 'Allow address', '0.0.0.0')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('monolog.handler.notification_console')->setOutput($output);

        $defaultPort = $this->getContainer()->getParameter('garant_ecm_notification.server.default_port');
        $defaultAddress = $this->getContainer()->getParameter('garant_ecm_notification.server.default_address');
        $defaultTcpPort = $this->getContainer()->getParameter('garant_ecm_notification.server.default_tcp_port');

        $port = $input->getOption('port') ?: $defaultPort;
        $address = $input->getOption('address') ?: $defaultAddress;

        $loop       = LoopFactory::create();
        $server     = $this->getContainer()->get('garant_ecm_notification.wamp_server');
        $server->setOutput($output);
        $serializer = $this->getContainer()->get('garant_ecm_notification.serializer');

        $handler = $this->getContainer()->get('session.handler');

        $context = new \React\ZMQ\Context($loop);
        $socket = $context->getSocket(\ZMQ::SOCKET_PULL);
        $socket->bind('tcp://127.0.0.1:'.$defaultTcpPort);
        $socket->on('message', function ($message) use ($server, $serializer, $output) {
            $output->writeln("<info>[Notification] {$message}</info>");
            $message = $serializer->deserialize($message, Socket\Message::class, 'json');
            call_user_func(array($server, Socket\PushInterface::PUSH_METHOD), $message);
        });
        $socket->on('error', function (\Exception $e) use ($output) {
            $output->writeln('<error>[Notification] ' . $e->getMessage() . '</error>');
        });

        $webSock = new \React\Socket\Server($loop);
        $webSock->listen($port, $address);

        $session = new SessionProvider(
            new \Ratchet\Wamp\WampServer($server)
            , $handler
            , array(
                'auto_start' => 1,
                'use_cookies' => 1,
            )
        );

        $webServer =
            new \Ratchet\Server\IoServer(
                new \Ratchet\Http\HttpServer(
                    new \Ratchet\WebSocket\WsServer(
                        $session
                    )
                ),
                $webSock
            );
        $output->writeln('<info>[Notification] Start WAMP server...</info>');
        $loop->run();
    }
}