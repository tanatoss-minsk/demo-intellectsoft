<?php

namespace Garant\ECM\Bundle\NotificationBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;


use Garant\ECM\Bundle\NotificationBundle\DependencyInjection\Compiler;

class GarantECMNotificationBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new Compiler\AddNotificationResolverPass());
        $container->addCompilerPass(new Compiler\ResolveTargetEntityPass());
    }
}
