<?php

namespace Garant\ECM\Bundle\NotificationBundle\Entity\Manager;

use Sonata\CoreBundle\Model\BaseEntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Garant\ECM\DomainModel\Organisation\Model\BaseEmployee;
use Garant\ECM\Bundle\NotificationBundle\Event\EventCollectorInterface;
use Garant\ECM\Bundle\NotificationBundle\Entity\NotificationEmployeeSetting;

/**
 * Class NotificationEmployeeSettingManager
 * @package Garant\ECM\Bundle\NotificationBundle\Entity\Manager
 */
class NotificationEmployeeSettingManager extends BaseEntityManager
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var EventCollectorInterface
     */
    protected $eventCollector;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $this->eventCollector = $container->get('garant_ecm_notification.event_collector');
    }

    /**
     * @param BaseEmployee $employee
     * @return NotificationEmployeeSetting
     */
    public function getSettingByEmployee(BaseEmployee $employee)
    {
        return $this->getRepository()->getSettingByEmployee($employee, $this->eventCollector->getEventsName());
    }

    public function saveSettingEmployee(NotificationEmployeeSetting $setting)
    {
        $settingWithService = array_unique(array_merge($setting->getSetting(), $this->eventCollector->getServiceEventsArray()));
        $setting->setSetting($settingWithService);
        $this->save($setting, true);
        return $setting;
    }
}