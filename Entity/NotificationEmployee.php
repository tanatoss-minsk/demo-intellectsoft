<?php

namespace Garant\ECM\Bundle\NotificationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Garant\ECM\Bundle\NotificationBundle\Entity\Notification;
use Garant\ECM\DomainModel\Organisation\Model\BaseEmployee as Employee;

/**
 * Class NotificationEmployee
 * @package Garant\ECM\Bundle\NotificationBundle\Entity
 *
 * @ORM\Table(name="ecm_notification_employees")
 * @ORM\Entity()
 */
class NotificationEmployee
{
    const STATUS_SENT = 1;

    const STATUS_PENDING = 2;

    const STATUS_EXPIRED = 3;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var Notification
     *
     * @ORM\ManyToOne(targetEntity="\Garant\ECM\Bundle\NotificationBundle\Entity\Notification", inversedBy="notificationEmployees")
     * @ORM\JoinColumn(name="notification_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $notification;

    /**
     * @var \Garant\ECM\Bundle\StaffBundle\Entity\BaseEmployee
     *
     * @ORM\ManyToOne(targetEntity="\Garant\ECM\DomainModel\Organisation\Model\BaseEmployee")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $employee;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", options={"default"=2})
     */
    private $status = self::STATUS_PENDING;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="process_date", type="datetime", nullable=false)
     */
    private $processDate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set notification
     *
     * @param \Garant\ECM\Bundle\NotificationBundle\Entity\Notification $notification
     * @return NotificationEmployee
     */
    public function setNotification(Notification $notification = null)
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * Get notification
     *
     * @return \Garant\ECM\Bundle\NotificationBundle\Entity\Notification
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Set employee
     *
     * @param Employee $employee
     * @return NotificationEmployee
     */
    public function setEmployee(Employee $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Notification
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set processDate
     *
     * @param \DateTime $processDate
     * @return Notification
     */
    public function setProcessDate(\DateTime $processDate = null)
    {
        $this->processDate = $processDate ?: new \DateTime();

        return $this;
    }

    /**
     * Get processDate
     *
     * @return \DateTime
     */
    public function getProcessDate()
    {
        return $this->processDate;
    }
}
