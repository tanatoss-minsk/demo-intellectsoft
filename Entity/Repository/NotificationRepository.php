<?php

namespace Garant\ECM\Bundle\NotificationBundle\Entity\Repository;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use Garant\ECM\DomainModel\Organisation\Model\BaseEmployee as Employee;
use Garant\ECM\Bundle\NotificationBundle\Entity\Repository\NotificationRepositoryInterface;

/**
 * Class NotificationRepository
 * @package Garant\ECM\Bundle\NotificationBundle\Entity\Repository
 */
class NotificationRepository  extends EntityRepository implements NotificationRepositoryInterface
{
    /**
     * @param Employee $employee
     * @return static
     */
    public function getQueryBuilderNotification(Employee $employee)
    {
        return $this->createQueryBuilder('notification')
            ->addSelect('notificationEmployee')
            ->join('notification.notificationEmployees', 'notificationEmployee')
            ->join('notificationEmployee.employee', 'employee')
            ->andWhere('employee = :employee')
            ->setParameter(':employee', $employee)
            ->orderBy('notification.createdAt', 'desc')
            ;
    }

    /**
     * @param Employee $employee
     * @param null $count
     * @param null $status
     * @return mixed
     */
    public function getNotificationByUser(Employee $employee, $count = null, $status = null)
    {
        $qb = $this->getQueryBuilderNotification($employee);
        if($count){
            $qb->setMaxResults($count);
        }
        if($status){
            $qb->andWhere('notificationEmployee.status = :status')
                ->setParameter('status', $status);
        }
        return $qb->getQuery()->getResult();
    }

    /**
     * @param QueryBuilder $qb
     * @param $event
     * @return QueryBuilder
     */
    public function filterByEvent(QueryBuilder $qb, $event)
    {
        return $qb
            ->andWhere('notification.event = :event')
            ->setParameter(':event', $event)
        ;
    }

    /**
     * @param QueryBuilder $qb
     * @param \DateTime $dateFrom
     * @return QueryBuilder
     */
    public function filterByDateFrom(QueryBuilder $qb, \DateTime $dateFrom)
    {
        return $qb
            ->andWhere('notification.createdAt > :dateFrom')
            ->setParameter(':dateFrom', $dateFrom)
            ;
    }

    /**
     * @param QueryBuilder $qb
     * @param \DateTime $dateTo
     * @return QueryBuilder
     */
    public function filterByDateTo(QueryBuilder $qb, \DateTime $dateTo)
    {
        return $qb
            ->andWhere('notification.createdAt < :dateTo')
            ->setParameter(':dateTo', $dateTo)
            ;
    }
}