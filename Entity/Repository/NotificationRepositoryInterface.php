<?php

namespace Garant\ECM\Bundle\NotificationBundle\Entity\Repository;

use Garant\ECM\DomainModel\Organisation\Model\BaseEmployee as Employee;
use Doctrine\ORM\QueryBuilder;

interface NotificationRepositoryInterface
{
    /**
     * @param Employee $employee
     * @return static
     */
    public function getQueryBuilderNotification(Employee $employee);

    /**
     * @param Employee $employee
     * @param null $count
     * @param null $status
     * @return mixed
     */
    public function getNotificationByUser(Employee $employee, $count = null, $status = null);

    /**
     * @param QueryBuilder $qb
     * @param $event
     * @return QueryBuilder
     */
    public function filterByEvent(QueryBuilder $qb, $event);

    /**
     * @param QueryBuilder $qb
     * @param \DateTime $dateFrom
     * @return QueryBuilder
     */
    public function filterByDateFrom(QueryBuilder $qb, \DateTime $dateFrom);

    /**
     * @param QueryBuilder $qb
     * @param \DateTime $dateTo
     * @return QueryBuilder
     */
    public function filterByDateTo(QueryBuilder $qb, \DateTime $dateTo);
}