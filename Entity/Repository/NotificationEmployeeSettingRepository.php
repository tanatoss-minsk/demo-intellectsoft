<?php

namespace Garant\ECM\Bundle\NotificationBundle\Entity\Repository;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Garant\ECM\DomainModel\Organisation\Model\BaseEmployee;
use Garant\ECM\Bundle\NotificationBundle\Entity\NotificationEmployeeSetting;

/**
 * Class NotificationEmployeeSettingRepository
 * @package Garant\ECM\Bundle\NotificationBundle\Entity\Repository
 */
class NotificationEmployeeSettingRepository extends EntityRepository
{
    /**
     * @param BaseEmployee $employee
     * @return NotificationEmployeeSetting|null|object
     */
    public function getSettingByEmployee(BaseEmployee $employee, $events = null)
    {

        if(!$setting = $this->findOneBy(['employee' => $employee])){
            $setting = new NotificationEmployeeSetting();
            $setting->setEmployee($employee);
            $setting->setSetting($events);
            $this->getEntityManager()->persist($setting);
        }
        return $setting;
    }
}