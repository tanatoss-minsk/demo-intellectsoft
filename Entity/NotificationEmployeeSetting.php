<?php

namespace Garant\ECM\Bundle\NotificationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Garant\ECM\Bundle\NotificationBundle\Entity\Notification;
use Garant\ECM\DomainModel\Organisation\Model\BaseEmployee as Employee;

/**
 * Class NotificationEmployeeSetting
 * @package Garant\ECM\Bundle\NotificationBundle\Entity
 *
 * @ORM\Table(name="ecm_notification_employees_setting")
 * @ORM\Entity(repositoryClass="Garant\ECM\Bundle\NotificationBundle\Entity\Repository\NotificationEmployeeSettingRepository")
 */
class NotificationEmployeeSetting
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \Garant\ECM\Bundle\StaffBundle\Entity\BaseEmployee
     *
     * @ORM\ManyToOne(targetEntity="\Garant\ECM\DomainModel\Organisation\Model\BaseEmployee")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $employee;

    /**
     * @var array $setting
     *
     * @ORM\Column(name="setting", type="array")
     */
    protected $setting;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getSetting()
    {
        return is_array($this->setting) ? $this->setting : [];
    }

    /**
     * @param array $setting
     */
    public function setSetting($setting)
    {
        $this->setting = $setting;
        return $this;
    }

    /**
     * Set employee
     *
     * @param Employee $employee
     * @return NotificationEmployee
     */
    public function setEmployee(Employee $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }
    
}