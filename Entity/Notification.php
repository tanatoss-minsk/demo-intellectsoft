<?php

namespace Garant\ECM\Bundle\NotificationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Garant\ECM\DomainModel\Notification\Model\Notification as NotificationModel;
use Garant\ECM\DomainModel\Notification\Model\NotificationType as NotificationTypeModel;
use Garant\ECM\Bundle\NotificationBundle\Entity\NotificationEmployee;
use Garant\ECM\Bundle\NotificationBundle\Event\EventCollector;

/**
 * Class Notification
 * @package Garant\ECM\Bundle\NotificationBundle\Entity
 *
 * @ORM\Table(name="ecm_notification")
 * @ORM\Entity(repositoryClass="Garant\ECM\Bundle\NotificationBundle\Entity\Repository\NotificationRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="entity_class", type="string")
 */
class Notification extends NotificationModel implements \JsonSerializable
{

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Garant\ECM\Bundle\NotificationBundle\Entity\NotificationEmployee", mappedBy="notification", cascade={"persist", "remove"})
     */
    protected $notificationEmployees;

    /**
     * @var string $config
     *
     * @ORM\Column(name="config", type="array")
     */
    protected $config;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notificationEmployees = new ArrayCollection();
    }

    public function jsonSerialize()
    {
        return [
            'id' => (int)$this->getId(),
            'event' => $this->getEvent(),
            'subject' => $this->getSubject(),
            'created_at' => (int) $this->getCreatedAt()->getTimestamp(),
            'updated_at' => ($this->getUpdatedAt()) ? (int)$this->getUpdatedAt()->getTimestamp() : null,
            'type' => isset($this->getConfig()[EventCollector::FIELD_TYPE]) ? $this->getConfig()[EventCollector::FIELD_TYPE] : null,
            'ttl' => isset($this->getConfig()[EventCollector::FIELD_TTL]) ? ( (int) $this->getConfig()[EventCollector::FIELD_TTL] ): null,
            'need_confirm' => isset($this->getConfig()[EventCollector::FIELD_NEED_CONFIRM]) ? ( ( bool) $this->getConfig()[EventCollector::FIELD_NEED_CONFIRM]) : false,
            'message_html' => isset($this->getConfig()['message_html']) ?  $this->getConfig()['message_html'] : null
        ];
    }

    /**
     * Add employees
     *
     * @param NotificationEmployee $notificationEmployee
     * @return Notification
     */
    public function addNotificationEmployees(NotificationEmployee $notificationEmployee)
    {
        if (!$this->notificationEmployees->contains($notificationEmployee)) {

            $this->notificationEmployees->add($notificationEmployee);
        }

        return $this;
    }

    /**
     * Remove employees
     *
     * @param NotificationEmployee $notificationEmployee
     */
    public function removeNotificationEmployee(NotificationEmployee $notificationEmployee)
    {
        $this->notificationEmployees->removeElement($notificationEmployee);
    }

    /**
     * Get employees
     *
     * @return \Garant\ECM\Bundle\NotificationBundle\Entity\NotificationEmployee[]
     */
    public function getNotificationEmployees()
    {
        return $this->notificationEmployees;
    }

    /**
     * @return string
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param string $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
        return $this;
    }
    
    public function createNotificationEmployee(\Garant\ECM\DomainModel\Organisation\Model\BaseEmployee $employee)
    {
        $notificationEmployee = new NotificationEmployee();
        $notificationEmployee->setNotification($this);
        $notificationEmployee->setEmployee($employee);
        $this->addNotificationEmployees($notificationEmployee);
    }
}
