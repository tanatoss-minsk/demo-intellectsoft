<?php

namespace Garant\ECM\Bundle\NotificationBundle\Notification;

use Symfony\Component\EventDispatcher\Event;
use Garant\ECM\Bundle\NotificationBundle\Entity\Notification;

/**
 * Interface ResolverInterface
 * @package Garant\ECM\Bundle\NotificationBundle\Notification
 */
interface ResolverInterface
{
    /**
     * @param Notification $notification
     * @return Notification|false
     */
    public function resolve(Event $event, Notification $notification);
}