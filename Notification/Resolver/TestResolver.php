<?php

namespace Garant\ECM\Bundle\NotificationBundle\Notification\Resolver;

use Garant\ECM\Bundle\NotificationBundle\Entity\Notification;
use Garant\ECM\Bundle\NotificationBundle\Notification\ResolverInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class TestResolver
 * @package Garant\ECM\Bundle\NotificationBundle\Notification\Resolver
 */
class TestResolver implements ResolverInterface
{
    /**
     * @param Notification $notification
     * @return Notification|false
     */
    public function resolve(Event $event, Notification $notification)
    {
        $notification->createNotificationEmployee($event->getSubject());
        return $notification;
    }
}