<?php

namespace Garant\ECM\Bundle\NotificationBundle\Notification\Resolver;

use Garant\ECM\Bundle\NotificationBundle\Entity\Notification;
use Garant\ECM\Bundle\NotificationBundle\Notification\ResolverInterface;
use Garant\ECM\Bundle\NotificationBundle\Event\EventCollectorInterface;
use Garant\ECM\Bundle\NotificationBundle\Entity\NotificationEmployeeSetting;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class EmployeeSettingResolver
 * @package Garant\ECM\Bundle\NotificationBundle\Notification\Resolver
 */
class EmployeeSettingResolver implements ResolverInterface
{
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var EventCollectorInterface
     */
    protected $eventCollector;

    /**
     * @param Registry $registry
     */
    public function __construct(Registry $registry, EventCollectorInterface $eventCollector)
    {
        $this->registry  = $registry;
        $this->eventCollector = $eventCollector;
    }

    /**
     * @param Notification $notification
     * @return Notification|false
     */
    public function resolve(Event $event, Notification $notification)
    {
        foreach($notification->getNotificationEmployees() as $notificationEmployee){
            $settingEmployee = $this->registry->getRepository(NotificationEmployeeSetting::class)->getSettingByEmployee($notificationEmployee->getEmployee(), $this->eventCollector->getEventsName());
            if(!in_array($notification->getEvent(), $settingEmployee->getSetting())){
                $notification->removeNotificationEmployee($notificationEmployee);
            }
        }
        return $notification;
    }
}