<?php

namespace Garant\ECM\Bundle\NotificationBundle\Form;

use CommonBundle\Form\Type\DatePicker;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Garant\ECM\Bundle\NotificationBundle\Event\EventCollectorInterface;

/**
 * Class FilterNotificationsType
 * @package Garant\ECM\Bundle\NotificationBundle\Form
 */
class FilterNotificationsType extends AbstractType
{
    /**
     * @var EventCollectorInterface
     */
    private $collector;

    public function __construct(EventCollectorInterface $collector)
    {
        $this->collector = $collector;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('events', ChoiceType::class,['choices' => $this->collector->getEventsArray(), 'multiple' => false, 'expanded'  => false, 'required' => false, 'label' => 'filter.events'])
            ->add('dateFrom', DatePicker::class, ['required' => false, 'label' => 'filter.dateFrom'])
            ->add('dateTo', DatePicker::class, ['required' => false, 'label' => 'filter.dateTo'])
        ;

        parent::buildForm($builder, $options);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'NotificationBundle',
        ));
    }
}