<?php

namespace Garant\ECM\Bundle\NotificationBundle\EventListener;

use Garant\ECM\Bundle\NotificationBundle\Entity\Notification;
use Garant\ECM\Bundle\NotificationBundle\Event\UnsentNotificationEvent;
use Garant\ECM\Bundle\NotificationBundle\Notification\ResolverInterface;
use Garant\ECM\Bundle\NotificationBundle\Event\NotificationEventInterface;;
use Garant\ECM\Bundle\NotificationBundle\Wamp\Socket\Message;
use Garant\ECM\Bundle\NotificationBundle\Event\EventCollectorInterface;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Routing\Router;
use React\ZMQ\SocketWrapper;
use React\EventLoop\Factory as LoopFactory;

/**
 * Class NotificationSubscriber
 * @package Garant\ECM\Bundle\NotificationBundle\EventListener
 */
class NotificationListener implements SerializerAwareInterface
{
    /**
     * @var ResolverInterface
     */
    protected $resolver;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var array
     */
    protected $socketContext;

    /**
     * @var EventCollectorInterface
     */
    protected $eventCollector;

    /**
     * NotificationListener constructor.
     * @param Registry $registry
     * @param ResolverInterface $resolver
     * @param Router $router
     * @param EventCollector $eventCollector
     */
    public function __construct(Registry $registry, ResolverInterface $resolver, Router $router, EventCollectorInterface $eventCollector)
    {
        $this->registry =  $registry;
        $this->resolver =  $resolver;
        $this->router = $router;
        $this->eventCollector = $eventCollector;
    }


    /**
     * @param NotificationEventInterface $event
     */
    public function onNotify(Event $event, $eventName)
    {
        if(!$event instanceof  \JsonSerializable){
            throw new \RuntimeException(sprintf('%s need implements \JsonSerializable', get_class($event)));
        }
        list ($socket, $loop) = $this->_getSocketContext();
        $manager = $this->registry->getManager();
        if($event instanceof UnsentNotificationEvent){
            $subject = $event->getSubject();
            $notification = $subject;
        }
        else{
            if(!$eventConfig = $this->eventCollector->getEvent($eventName))
            {
                return;
            }
            $notification = new Notification();
            $notification->setEvent($eventName);
            $notification->setConfig($eventConfig);
            $notification->setSubject($event);

            // Резолвим уведомление
            $notification = $this->resolver->resolve($event, $notification);
        }
        if ($notification->getNotificationEmployees()->count()) {
            $topics = array();
            $notificationEmployees = $notification->getNotificationEmployees();
            foreach ($notificationEmployees as $notificationEmployee) {

                $employee = $notificationEmployee->getEmployee();
                $topics = array_merge(
                    $topics,
                    ['/'.$this->router->generate('notification_topic', ['employee' => $employee->getId()], Router::RELATIVE_PATH)]
                );
                $notificationEmployee->setProcessDate(new \DateTime());
                $manager->persist($notificationEmployee);
            }
            $manager->persist($notification);
            $manager->flush();
            // Exclude duplicates
            $topics = array_values(array_unique($topics));
            // Отправляем серверу уведомлений
            $socket->send($this->serializer->serialize(new Message($topics, $notification->getId()), 'json'));
        } // if
    }

    /**
     * @return array|\React\ZMQ\SocketWrapper
     */
    protected function _getSocketContext()
    {
        if (!$this->socketContext) {
            $loop = LoopFactory::create();
            $context = new \ZMQContext();
            /**
             * @var \React\ZMQ\SocketWrapper $socket
             */
            $socket = $context->getSocket(\ZMQ::SOCKET_PUSH);
            $socket->connect("tcp://localhost:5555");
            $this->socketContext = array($socket, $loop);
        }

        return $this->socketContext;
    }

    /**
     * @param SerializerInterface $serializer
     * @return $this
     */
    public function setSerializer(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;

        return $this;
    }
}