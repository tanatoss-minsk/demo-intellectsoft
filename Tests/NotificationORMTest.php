<?php

namespace Garant\ECM\Bundle\NotificationBundle\Tests;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\ORM\EntityManager;

use Garant\ECM\Bundle\NotificationBundle\Entity\Notification;
use Garant\ECM\Bundle\NotificationBundle\Entity\NotificationEmployee;
use Garant\ECM\Bundle\NotificationBundle\Entity\NotificationEmployeeSetting;
use Garant\ECM\Bundle\NotificationBundle\Event\DemoNotificationEvent;
use Garant\ECM\Bundle\NotificationBundle\Event\UnsentNotificationEvent;
use Garant\ECM\DomainModel\Organisation\Model\BaseEmployee;


class NotificationORMTest  extends WebTestCase
{
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var EntityManager
     */
    private $em;

    protected $container;

    /**
     * @var BaseEmployee
     */
    protected $user;

    const USER_PROPERTY = 'superAdmin';

    public function setUp()
    {
        parent::setUp();
        static::bootKernel();
        $this->container = static::$kernel->getContainer();
        $this->doctrine = static::$kernel
            ->getContainer()
            ->get('doctrine');
        $this->doctrine->getConnection()->beginTransaction();
        $this->em = $this->doctrine->getManager();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     *  route garant_ecm_notification_test_send create DemoNotificationEvent,
     *  which sends a message to the user specified in the subject
     */
    public function testNotification()
    {
        $this->getEmployee();
        static::$kernel
            ->getContainer()->get('event_dispatcher')->dispatch(
            DemoNotificationEvent::NAME,
            new DemoNotificationEvent($this->user)
        );

        $repoNotification = $this->em->getRepository('GarantECMNotificationBundle:Notification');
        $repoNotificationEmployee = $this->em->getRepository('GarantECMNotificationBundle:NotificationEmployee');

        $notification = $repoNotification->findOneBy(['event' => DemoNotificationEvent::NAME]);
        $this->assertNotEmpty($notification);
        $this->assertEquals($notification->getEvent(), DemoNotificationEvent::NAME);

        $notificationEmployee = $repoNotificationEmployee->findOneBy(['employee' => $this->user]);
        $this->assertEquals($notificationEmployee->getNotification()->getId(), $notification->getId());
        $this->assertEquals($notificationEmployee->getEmployee()->getId(), $this->user->getId());

        $this->assertNotEmpty($notificationEmployee);

        $settingManager = $this->container->get('garant_ecm_notification.manager.notification_employee_setting');
        $setting = $settingManager->getSettingByEmployee($this->user);
        $setting = $settingManager->saveSettingEmployee($setting);
        $this->assertTrue(in_array(DemoNotificationEvent::NAME, $setting->getSetting()));
        $this->assertTrue(in_array(UnsentNotificationEvent::NAME, $setting->getSetting()));
    }


    private function getEmployee()
    {
        if(!$this->user = $this->doctrine->getRepository(
            $this->container->getParameter('garant_ecm_notification.employee_entity_class')
        )->findOneBy(['username' => self::USER_PROPERTY])){
            $userClassName = $this->container->getParameter('garant_ecm_notification.employee_entity_class');
            $this->user = new $userClassName();
            $this->user->setUsername(self::USER_PROPERTY);
            $this->user->setPlainPassword(self::USER_PROPERTY);
            $this->user->setEmail('supertester@mail.com');
            $this->user->setEnabled(true);
            $this->user->setSuperAdmin(true);
            $this->user->setLocked(false);
            $this->em->persist($this->user);
        }
        $settingEmployee = $this->doctrine->getRepository(NotificationEmployeeSetting::class)->getSettingByEmployee($this->user);
        $settingEmployee->setSetting([DemoNotificationEvent::NAME]);
        $this->em->persist($settingEmployee);
        $this->em->flush();
    }
}