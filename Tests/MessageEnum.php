<?php


namespace Garant\ECM\Bundle\NotificationBundle\Tests;


class MessageEnum
{
    const MESSAGE_CONNECT_FAIL      = 'Connection fail';
    const MESSAGE_CONNECT_SUCCESS   = 'Connection success';
    const CONNECTION_CONTAINER      = 'connectionContainer';
    const TOPIC_CONTAINER           = 'topicContainer';
    const SUBSCRIBER_DATA_CONTAINER = 'subscriberDataContainer';
    const SUBSCRIBER_DATA_ID        = 'id';
}