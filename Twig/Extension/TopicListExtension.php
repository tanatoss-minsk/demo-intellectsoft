<?php

namespace Garant\ECM\Bundle\NotificationBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Router;

class TopicListExtension extends \Twig_Extension implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'notification.topic_list';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('notification_topic_list', [$this, 'getTopicList'])
        ];
    }

    public function getTopicList()
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if(!is_object($user)){
            return '[]';
        }
        $userId = $this->container->get('security.token_storage')->getToken()->getUser()->getId();
        $topic = '/'.$this->container->get('router')->generate('notification_topic', ['employee' => $userId], Router::RELATIVE_PATH);
        return '["'.$topic.'"]';
    }
}