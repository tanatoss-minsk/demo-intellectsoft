<?php
/**
 * Created by PhpStorm.
 * User: tanatoss
 * Date: 19.6.16
 * Time: 15.46
 */

namespace Garant\ECM\Bundle\NotificationBundle\Wamp;

use Symfony\Component\Routing\Router;

/**
 * Interface ChatServerInterface
 * @package Garant\ECM\Bundle\NotificationBundle\Wamp
 */
interface  NotificationServerInterface
{

    /**
     * @return Employee
     */
    public function getCallUser();

    /**
     * @return \SplObjectStorage
     */
    public function getOnlineUsers();
}