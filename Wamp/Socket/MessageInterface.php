<?php

namespace Garant\ECM\Bundle\NotificationBundle\Wamp\Socket;

/**
 * Interface MessageInterface
 * @package Garant\ECM\Bundle\NotificationBundle\Wamp\Socket
 */
interface MessageInterface
{
    /**
     * @return string
     */
    public function getTopics();

    /**
     * @return string
     */
    public function getContent();
}