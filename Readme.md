# ECM Notification Bundle #

This bundle for messaging between the user and the server.

## Installation

### With composer

This bundle can be installed using [composer](https://getcomposer.org/):

    composer require garant/ecm-notification-bundle

### Register the bundle

    <?php

    // app/AppKernel.php

    public function registerBundles()
    {
        $bundles = array(

            // ...
            new Garant\ECM\Bundle\NotificationBundle\GarantECMNotificationBundle()
            // ...
        );

    	// ...
    }

### Configuration

### New monolog channel

    monolog:
        channels:
            - notification
    
        handlers:
            notification_console:
                type: console
                channels: [notification]
    
            notification_stream:
                type:     stream
                path:     %kernel.logs_dir%/notifications.log
                channels: [notification]

    # Project host It coincides with the Ratchet server host
    ecm.notification.server.default_host:
    
    ecm.notification.server.default_port:

### Commands

`garant:ecm:notification:server` - start Ratchet serve

### Services

`garant_ecm_notification.wamp_server` - WAMP server. It describes the basic sever methods. (onOpen, onPush, onCall)

`garant_ecm_notification.topic_generator` - Create topics using the standard router

`garant_ecm_notification.resolver` - Broker for resolvers to event

`garant_ecm_notification.notification_listener` - listener for notification events. Create and sends message to the Ratchet server


### Manual
#### **Что надо что-бы создать  нотификацию**
1. **Запустить сервер  сообщений**
1. **Создать Event**
2. **Создать Resolver**
3. **Вызвать событие с этим event**


#### Example

Event - Garant\ECM\Bundle\NotificationBundle\Event\DemoNotificationEvent
- с интерфейсом NotificationEventInterface. Клиенту отдается сериализованый (jsonSerialize) event - там должна быть вся необходимая
 информация. Этот event должен быть описан в app/config/garant/ecm

 1. type - тип (info, warning, error, success)

 2. need_confirm - необходимость подтверждения получения нотификации юзером. Если не подтвердить, будет приходить каждый раз

 3. ttl - время показа в миллисекундах

 4. template - необязательный параметр. Если указать, то клиенту будет передан отрендереный шаблон, в который будет передан сериализованный event


```
garant_ecm_notification
    events_parameters:
      - { event: NOTIFICATION_TEST, type: warning, need_confirm: true, ttl: 5000, template:  GarantECMNotificationBundle:Event:event_demo.html.twig }
```

Resolver - Garant\ECM\Bundle\NotificationBundle\Notification\Resolver\TestResolver - тут добавить юзеров  для получения нотификаций. Если необходимо,
то можно поменять контекст ($notification->setSubject($newContext))
```
   garant_ecm_notification.resolver.unsent_notification_resolver:
        class: %garant_ecm_notification.resolver.unsent_notification_resolver.class%
        public: false
        tags:
            - { name: notification.resolver, notification: ecm.notification.unsent }
```









