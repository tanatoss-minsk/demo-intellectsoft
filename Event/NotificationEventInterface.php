<?php

namespace Garant\ECM\Bundle\NotificationBundle\Event;


interface NotificationEventInterface
{
    public function jsonSerialize();
    
    public static function getName();
}