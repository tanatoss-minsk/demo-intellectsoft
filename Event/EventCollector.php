<?php

namespace Garant\ECM\Bundle\NotificationBundle\Event;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Garant\ECM\Bundle\NotificationBundle\Event\EventCollectorInterface;

/**
 * Class EventCollector
 * @package Garant\ECM\Bundle\NotificationBundle\Event
 */
class EventCollector implements EventCollectorInterface
{
    /**
     * @var array
     */
    protected $events = [];

    /**
     * @var ContainerInterface
     */
    protected $container;

    const FIELD_TEMPLATE         = 'template';
    const FIELD_TYPE             = 'type';
    const FIELD_TTL              = 'ttl';
    const FIELD_NEED_CONFIRM     = 'need_confirm';
    const FIELD_EVENT            = 'event';
    const FIELD_TRANSLATE_BUNDLE = 'translate_bundle';
    const FIELD_SERVICE          = 'service';

    /**
     * EventCollector constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $eventParams = $container->getParameter('garant_ecm_notification.events_parameters');
        foreach ($eventParams as $eventParam){
            $this->events[$eventParam['event']] = array_merge($eventParam, [
                'translateName' => $this->container->get('translator')->trans($eventParam[self::FIELD_EVENT], [], $eventParam[self::FIELD_TRANSLATE_BUNDLE])
            ]);;
        }
    }

    /**
     * @param string $serviceId
     * @param array $tags
     */
    public function addEvent($serviceId, $tags)
    {
        if(isset($tags[self::FIELD_EVENT])){
            $this->events[$tags[self::FIELD_EVENT]] = array_merge($tags, [
                'service' => $serviceId,
                'translateName' => $this->container->get('translator')->trans($tags[self::FIELD_EVENT], null, $tags[self::FIELD_TRANSLATE_BUNDLE])
            ]);
        }
    }

    /**
     * @param string $eventName
     * @return array
     */
    public function getEvent($eventName)
    {
        if(isset($this->events[$eventName])){
            return $this->events[$eventName];
        }
    }

    /**
     * @return array
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @return array
     */
    public function getEventsName()
    {
        return array_keys($this->events);
    }

    /**
     * @return array
     */
    public function getEventsArray()
    {
        $choices = [];
        foreach ($this->getEvents() as $event) {
            if($this->container->get('kernel')->getEnvironment() != 'dev' && $event['service']){
                continue;
            }
            else{
                $choices[$event['translateName']] = $event['event'];
            }
        }
        return $choices;
    }

    /**
     * @return array
     */
    public function getServiceEventsArray()
    {
        $choices = [];
        foreach ($this->getEvents() as $event) {
            if($event['service']){
                $choices[] = $event['event'];
            }
        }
        return $choices;
    }
}