<?php

namespace Garant\ECM\Bundle\NotificationBundle\Event;


interface EventCollectorInterface
{
    /**
     * @param string $serviceId
     * @param array $tags
     */
    public function addEvent($serviceId, $tags);

    /**
     * @param string $eventName
     * @return array
     */
    public function getEvent($eventName);

    /**
     * @return array
     */
    public function getEvents();

    /**
     * @return array
     */
    public function getEventsName();

    /**
     * @return array
     */
    public function getEventsArray();

    /**
     * @return array
     */
    public function getServiceEventsArray();
}