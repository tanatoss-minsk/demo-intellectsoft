<?php

namespace Garant\ECM\Bundle\NotificationBundle\Event;

use Symfony\Component\EventDispatcher\GenericEvent;
use Garant\ECM\DomainModel\Organisation\Model\BaseEmployee as Employee;
use Garant\ECM\Bundle\NotificationBundle\Traits\EventTypeTrait;
use Garant\ECM\Bundle\NotificationBundle\Notification\SubjectNotificationEvent;


/**
 * Class DemoNotificationEvent
 * @package Garant\ECM\Bundle\NotificationBundle\Event
 */
class DemoNotificationEvent extends GenericEvent implements  \JsonSerializable
{
    use EventTypeTrait;
    
    const NAME = 'NOTIFICATION_TEST';

    /**
     * DemoNotificationEvent constructor.
     * @param Employee $notification
     * @param array $arguments
     */
    public function __construct(Employee $employee = null, array $arguments = array())
    {
        parent::__construct($employee, $arguments);
    }

    public static function getName()
    {
        return self::NAME;
    }

    public function jsonSerialize()
    {
        return [
            'subject' => $this->getSubject()
        ];
    }
}