<?php

namespace Garant\ECM\Bundle\NotificationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use Garant\ECM\Bundle\NotificationBundle\Event\DemoNotificationEvent;

class TestController  extends Controller
{
    /**
     * @Route(name="garant_ecm_notification_test", path="/garant_nffotification_test/{id}", requirements={"id" = "\d+"})
     */
    public function indexAction($id)
    {
        return $this->render('GarantECMNotificationBundle:Test:test.html.twig',
            [
                'notification_server_default_host' => $this->getParameter('garant_ecm_notification.server.default_host'),
                'notification_server_default_port' => $this->getParameter('garant_ecm_notification.server.default_port'),
                'user' => $this->getDoctrine()->getRepository($this->getParameter('garant_ecm_notification.employee_entity_class'))->find($id)
            ]
        );
    }

    /**
     * @Route(name="garant_ecm_notification_test_send", path="/garant_notification_test_send/{id}", requirements={"id" = "\d+"})
     */
    public function sendAction($id)
    {
        $subject = $this->getDoctrine()->getRepository($this->getParameter('garant_ecm_notification.employee_entity_class'))->find($id);
        $this->get('event_dispatcher')->dispatch(
            DemoNotificationEvent::getName(),
            new DemoNotificationEvent($subject)
        );
        return new JsonResponse(['result' => true]);
    }
}