<?php

namespace Garant\ECM\Bundle\NotificationBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\QueryBuilder;
use Garant\ECM\Bundle\NotificationBundle\Form\FilterNotificationsType;
use Garant\ECM\Bundle\NotificationBundle\Entity\Repository\NotificationRepositoryInterface;

/**
 * Class DefaultController
 * @package Garant\ECM\Bundle\NotificationBundle\Controller
 * @Route()
 */
class DefaultController extends Controller
{
    const RECORDS_IN_LIST = 10;

    /**
     * Return all emails from folder
     *
     * @param Request $request
     * @Route("/list", name="garant_notification_list")
     * @Template()
     * @return array
     */
    public function listAction(Request $request)
    {

        $form = $this->createForm(FilterNotificationsType::class);
        $form->handleRequest($request);

        $notificationRepository = $this->get('doctrine')->getManager()->getRepository('GarantECMNotificationBundle:Notification');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $qb = $notificationRepository->getQueryBuilderNotification($user);
        $qb = $this->setFilter($qb, $notificationRepository, $form->getData());

        $query = $qb->getQuery();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            self::RECORDS_IN_LIST
        );
        return ['pagination' => $pagination, 'form' => $form->createView()];
    }

    /**
     * @param QueryBuilder $qb
     * @param NotificationRepositoryInterface $repository
     * @param $filterData
     */
    private function setFilter(QueryBuilder $qb, NotificationRepositoryInterface $repository, $filterData)
    {
        if($filterData['events']){
            $qb = $repository->filterByEvent($qb, $filterData['events']);
        }
        if($filterData['dateFrom']){
            $qb = $repository->filterByDateFrom($qb, $filterData['dateFrom']);
        }
        if($filterData['dateTo']){
            $qb = $repository->filterByDateTo($qb, $filterData['dateTo']);
        }
        return $qb;
    }
}