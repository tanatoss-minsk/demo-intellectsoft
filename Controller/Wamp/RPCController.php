<?php

namespace Garant\ECM\Bundle\NotificationBundle\Controller\Wamp;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Garant\ECM\Bundle\NotificationBundle\Topic\Generator;
use Garant\ECM\Bundle\NotificationBundle\Entity\Notification;
use Garant\ECM\Bundle\NotificationBundle\Entity\NotificationEmployee;
use Garant\ECM\Bundle\NotificationBundle\Event\UnsentNotificationEvent;
use Garant\ECM\Bundle\NotificationBundle\Wamp\NotificationServerInterface;
use Garant\ECM\Bundle\APIBundle\Traits\ResponseTrait;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RPCController
 * @package Garant\ECM\Bundle\NotificationBundle\Controller\Wamp
 */
class RPCController extends Controller
{
    use ResponseTrait;

    /**
     * @param Request $request
     * @throws \Exception
     * @return array
     */
    public function getTopicListAction(Request $request, NotificationServerInterface $server)
    {
        $topics = array();
        $user = $server->getCallUser();
        if ($user) {

            $em = $this->getDoctrine()->getManager();

            /**
             * @var Employee $employee
             */
            $employee = $em->getRepository($this->getParameter('garant_ecm_notification.employee_entity_class'))
                ->createQueryBuilder('e')
                ->where('e.id = :employee_id')
                ->setParameter('employee_id', $user->getId())
                ->getQuery()
                ->getOneOrNullResult()
            ;

            if ($employee) {
                $topics = $this->get('garant_ecm_notification.topic_generator')
                    ->generate(new Generator\Strategy\EmployeeStrategy($employee));
            }
        } // if
        return $this->response($topics);
    }

    /**
     * @param Request $request
     * @param NotificationServerInterface $server
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function checkUnsentNotificationsAction(Request $request, NotificationServerInterface $server)
    {
        $user = $server->getCallUser();

        if ($user) {
            $em =   $this->getDoctrine()->getManager();
            $notifications = $em->getRepository('GarantECMNotificationBundle:Notification')
                ->createQueryBuilder('notification')
                ->join('notification.notificationEmployees', 'notificationEmployees')
                ->join('notificationEmployees.employee', 'employee')
                 ->where('employee.id = :employee_id')
                ->setParameter('employee_id', $user->getId())
                ->andWhere('notificationEmployees.status = :status')
                ->setParameter('status', NotificationEmployee::STATUS_PENDING)
                ->getQuery()
                ->getResult()
            ;
            
            /**
             * @var Notification $notification
             */
            foreach ($notifications as $notification) {
                $this->get('event_dispatcher')->dispatch(
                    UnsentNotificationEvent::getName(),
                    new UnsentNotificationEvent($notification)
                );
            } // foreach
        } // if
        return $this->response(null);
    }


    /**
     * @param Request $request
     * @param NotificationServerInterface $server
     * @Route("/notifications/confirm", name="notification_confirm_read")
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function confirmAction(Request $request, NotificationServerInterface $server)
    {
        $user = $server->getCallUser();
        $em =   $this->getDoctrine()->getManager();
        if($user && $request->get('params', [])){
            $ids = json_decode($request->get('params')[0], true);
            if(is_array($ids[0])){
                $ids = $ids[0];
            }
            if(is_array($ids)){
                foreach ($ids as $id){
                    $notification = $em->getRepository('GarantECMNotificationBundle:Notification')->find($id);
                    $notificationEmployee = $em->getRepository('GarantECMNotificationBundle:NotificationEmployee')->findOneBy(['employee' => $user, 'notification' => $notification]);
                    if($notificationEmployee){
                        $notificationEmployee->setStatus(NotificationEmployee::STATUS_SENT);
                        $em->persist($notificationEmployee);
                    }
                }
                $em->flush();
                return $this->response(true);
            }
        }
        return $this->error('not exist notification');
    }
}


























